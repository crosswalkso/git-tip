# Git-tip

## vscode terminal
```
^(Ctrl) + `
```

## Gitlab - Github 로그인 연동 문제
Github으로 로그인 할 경우 GitLab은 비밀번호 정보를 알지 못하여 터미널에서 비밀번호를 입력하여도 push를 할 수 없다.

### 해결 방법
우측 상단 GitLab 계정의 Edit profile - Password에 들어가서 비밀번호를 한 번 설정해줘야 한다.

## Github - PW대신 token 사용
계정 Settings 맨 아래 Developer settings - Personal access tokens - 체크하고 발급.  
다시 볼 수 없으니까 저장해두기. ID/PW 입력 시 PW대신 token 입력

## Github Pages
저장소 Settings - pages - branch 설정하고 save - action에서 확인

## add 취소하기
```
git reset HEAD [파일명.확장자]
```

## [프로젝트마다 다른 계정으로 커밋하기](https://2dowon.github.io/docs/etc/commit-by-different-account/)
```
git config --global user.name "이름"
git config --global user.email "이메일"
```
- 기본 커밋 아이디
- 다른 계정으로 커밋하겠다고 `--global`을 건드리면 앞으로 바꾼 계정으로 계속 커밋 된다.
### 특정 project나 repository에서만 다른 계정으로 commit, push
```
git config --local user.name "현재 레포에서 사용할 이름"
git config --local user.email "현재 레포에서 사용할 이메일"
```
- `--local` 설정을 건드리면 된다.
- 저장소가 설정된 폴더에서 (`.git` 폴더가 있는) `config git --list`하면 global 계정과 그 폴더에서만 사용할 계정이 표시된다.

# SSH Key
### [참고자료](https://velog.io/@sonypark/GitHubSSH%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%B4-%EC%97%AC%EB%9F%AC%EA%B0%9C%EC%9D%98-%EA%B9%83%ED%97%88%EB%B8%8C-%EA%B3%84%EC%A0%95-%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0-6mk3iesh0u)
## 1. SSH key 생성
```
cd ~/.ssh # .ssh 폴더로 이동
```
`.ssh` 폴더 내에서 SSH key 관리가 이루어진다.
```
ssh-keygen -t rsa -C "your_email@example.com"
```
깃허브 계정 생성 시 입력해준 이메일을 적는다.
### 파일명
나중에 폴더 내에서 파일명을 바꾸면 동작하지 않으니까 처음부터 알아보기 쉽게 지어준다.  
ex) `id_rsa_(account_name)`
## 2. Github/GitLab 설정
### .pub
`.pub` 파일을 텍스트 편집기로 열고 github, gitlab의 계정 setting란으로 가서 SSH key를 입력해준다.

## 3. config 설정
다음과 같이 config 파일에 경로를 저장해주면 SSH key를 여러 개 사용할 수 있다.  
account_name은 혹시 모르니까 파일 생성 시 입력한 이름과 통일하도록 하자.
```
touch config
nano config

Host github.com-(account_name)
    HostName github.com
    User git
    IdentityFile ~/.ssh/id_rsa_(account_name)

Host ...
```
#### Host
저장소 주소를 불러오는 Key  
`.git/` -> `config`
```
[remote "origin"]
	url = git@github.com|-(account_name)|:{github_id}/{repository_name}.git
```
연결할 저장소의 깃 파일의 config를 열고 `|-(account_name)|`이 부분을 추가해주자. | 문자는 지우기.